# noto-fonts-emoji-apple
### Same as `https://gitlab.com/timescam/noto-fonts-emoji-apple` but emojes from iOS 14
Google Noto emoji fonts replaced with apple branded emoji, modified form a magisk module.
`https://forum.xda-developers.com/apps/magisk/magisk-ios-13-2-emoji-t3993487`

you can get it on [aur](https://aur.archlinux.org/packages/noto-fonts-emoji-apple/)
